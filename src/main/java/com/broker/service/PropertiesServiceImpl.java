package com.broker.service;

import java.util.List;

import com.broker.model.Property;
import com.broker.repository.IPropertiesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PropertiesServiceImpl implements IPropertiesService {

    private IPropertiesRepository propertiesRepository;

    @Autowired
    public void setIPersonPropertiesRepositoy(IPropertiesRepository propertiesRepository) {
        this.propertiesRepository = propertiesRepository;
    }

    public void addProperty(Property property) {
        propertiesRepository.insert(property);
        System.out.println("Property has been added!");
    }

    public void deleteProperty(int id) {
        propertiesRepository.deleteById(id);
    }

    public void updateProperty(int id, int price) {
    }

    public List<Property> getAll() {
        List<Property> properties = propertiesRepository.findAll();
        System.out.println("All properties has been fetched!\n\n");
        return properties;
    }

    // public Property getById(Integer id) {
    // Property property = propertiesRepository.findById(id);
    // return property;
    // }

    @Override
    public List<Property> getByType(String type) {
        List<Property> properties = propertiesRepository.findByType(type);
        System.out.println("Properties by specific type : " + type + "\n\n");
        return properties;
    }

    @Override
    public List<Property> getByPrice(int price) {
        List<Property> properties = propertiesRepository.findByPrice(price);
        System.out.println("Properties with price lesser than this : " + price + "\n\n");
        return properties;
    }

    @Override
    public List<Property> getByBrokrage(int brokrage) {
        List<Property> properties = propertiesRepository.findByBrokrage(brokrage);
        System.out.println("Properties with brokrage lesser than this : " + brokrage + "\n\n");
        return properties;
    }

    @Override
    public List<Property> getByMaxDiscount(int maxDiscount) {
        List<Property> properties = propertiesRepository.findByMaxDiscount(maxDiscount);
        System.out.println("properties with maximum discount not greater than this: " + maxDiscount + "\n\n");
        return properties;
    }

    @Override
    public List<Property> getByIsBrokrageApplicable(Boolean isBrokrageApplicable) {
        List<Property> properties = propertiesRepository.findByIsBrokrageApplicable(isBrokrageApplicable);
        System.out.println("Is brokrage applicable : " + isBrokrageApplicable + "\n\n");
        return properties;
    }

    @Override
    public List<Property> getByTypeAndMaxDiscount(String type, int maxDiscount) {
        List<Property> properties = propertiesRepository.findByTypeAndMaxDiscount(type, maxDiscount);
        System.out.println("Properties with type : " + type + " and discount not greater than " + maxDiscount + "\n\n");
        return properties;
    }

    @Override
    public List<Property> getByTypeAndPriceAndMaxDiscount(String type, int price, int maxDiscount) {
        List<Property> properties = propertiesRepository.findByTypeAndPriceAndMaxDiscount(type, price, maxDiscount);
        System.out.println("Properties with type : " + type + " , price not greater than " + price
                + " and discount not greater than " + maxDiscount + "\n\n");
        return properties;
    }

    @Override
    public List<Property> getByPriceAndIsBrokrageApplicable(int price, Boolean isBrokrageApplicable) {
        List<Property> properties = propertiesRepository.findByPriceAndIsBrokrageApplicable(price,
                isBrokrageApplicable);
        System.out.println(
                "Properties by price greater than " + price + " and is brokrage applicable" + isBrokrageApplicable
                        + "\n\n");
        return properties;
    }

}
