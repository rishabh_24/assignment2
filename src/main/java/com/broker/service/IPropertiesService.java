package com.broker.service;

import java.util.List;

import com.broker.model.Property;

public interface IPropertiesService {
    void addProperty(Property property);

    void deleteProperty(int id);

    void updateProperty(int id, int price);

    List<Property> getAll();

    List<Property> getByType(String type);

    List<Property> getByPrice(int price);

    List<Property> getByBrokrage(int brokrage);

    List<Property> getByMaxDiscount(int maxDiscount);

    List<Property> getByIsBrokrageApplicable(Boolean isBrokrageApplicable);

    List<Property> getByTypeAndMaxDiscount(String type, int maxDiscount);

    List<Property> getByTypeAndPriceAndMaxDiscount(String type, int price, int maxDiscount);

    List<Property> getByPriceAndIsBrokrageApplicable(int price, Boolean isBrokrageApplicable);
}
