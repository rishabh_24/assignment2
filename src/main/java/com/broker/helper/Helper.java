package com.broker.helper;

import com.broker.model.Property;

public class Helper {
    private static String[] types = { "plot", "flat" };

    public static String getPropertType() {
        return types[(int) (1e3 * Math.random()) % 2];
    }

    public static Property propertyMaker(int id) {
        int rand = (int) (1000 * Math.random());
        String type = types[(rand % 2)];
        int discount = (int) (10 * Math.random());
        int price = 10000 + (int) (1e6 * Math.random());
        int brokrage = rand % 2 == 1 ? (int) (1000 * Math.random()) : 0;
        Boolean isBrokrageApplicable = rand % 2 == 1 ? true : false;
        return new Property(id, type, price, discount, brokrage, isBrokrageApplicable);
    }
}
