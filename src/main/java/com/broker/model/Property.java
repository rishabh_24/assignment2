package com.broker.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Document
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Property {

    private Integer id;
    private String type;
    private int price;
    private int maxDiscount;
    private int brokrage;
    private Boolean isBrokrageApplicable;
}
