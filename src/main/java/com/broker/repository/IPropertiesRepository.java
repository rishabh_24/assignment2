package com.broker.repository;

import java.util.List;

import com.broker.model.Property;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IPropertiesRepository extends MongoRepository<Property, Integer> {

    @Query("{type : ?0}")
    List<Property> findByType(String type);

    @Query("{price: {$lte : ?0}}")
    List<Property> findByPrice(int price);

    @Query("{brokrage : {$lte : ?0}}")
    List<Property> findByBrokrage(int brokrage);

    @Query("{maxDiscount : {$lte : ?0}}")
    List<Property> findByMaxDiscount(int maxDiscount);

    @Query("{isBrokrageApplicable : ?0}")
    List<Property> findByIsBrokrageApplicable(Boolean isBrokrageApplicable);

    @Query("{$and : [{type : ?0}, {maxDiscount : {$lte : ?1}}]}")
    List<Property> findByTypeAndMaxDiscount(String type, int maxDiscount);

    @Query("{$and : [{type : ?0},{price :{$lte :?1}}, {maxDiscount :{$lte : ?2}}]}")
    List<Property> findByTypeAndPriceAndMaxDiscount(String type, int price, int maxDiscount);

    @Query("{$and:[{price : {$gte : ?0 }}, {isBrokrageApplicable : ?1 }]}")
    List<Property> findByPriceAndIsBrokrageApplicable(int price, Boolean isBrokrageApplicable);
}
