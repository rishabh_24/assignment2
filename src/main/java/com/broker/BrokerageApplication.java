package com.broker;

import com.broker.helper.Helper;
import com.broker.service.IPropertiesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrokerageApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(BrokerageApplication.class, args);
	}

	@Autowired
	IPropertiesService propertiesService;

	@Override
	public void run(String... args) throws Exception {
		// for (int i = 0; i < 1e4; i++) {
		// propertiesService.addProperty(Helper.propertyMaker(i + 1));
		// }

		// propertiesService.getAll().stream().forEach(System.out::println);

		int propertyToBeDeleted = (int) (1e4 * Math.random());
		propertiesService.deleteProperty(propertyToBeDeleted);

		int propertyToBeUpdated = (int) (1e24 * Math.random());
		int price = (int) (1e5 * Math.random());
		propertiesService.updateProperty(propertyToBeUpdated, price);

		String type = Helper.getPropertType();
		propertiesService.getByType(type).stream().forEach(System.out::println);

		price = (int) (1e6 * Math.random());
		propertiesService.getByPrice(price).stream().forEach(System.out::println);

		int brokrage = (int) (1e3 * Math.random());
		propertiesService.getByBrokrage(brokrage).stream().forEach(System.out::println);

		int discount = (int) (10 * Math.random());
		propertiesService.getByMaxDiscount(discount).stream().forEach(System.out::println);

		Boolean propertiesWithBrokrage = true;
		propertiesService.getByIsBrokrageApplicable(propertiesWithBrokrage).stream().forEach(System.out::println);

		Boolean propertiesWithoutBrokrage = false;
		propertiesService.getByIsBrokrageApplicable(propertiesWithoutBrokrage).stream().forEach(System.out::println);

		type = Helper.getPropertType();
		discount = (int) (10 * Math.random());
		propertiesService.getByTypeAndMaxDiscount(type, discount).stream().forEach(System.out::println);

		price = (int) (1e6 * Math.random());
		propertiesService.getByPriceAndIsBrokrageApplicable(price, propertiesWithBrokrage).stream()
				.forEach(System.out::println);

		price = (int) (1e6 * Math.random());
		propertiesService.getByPriceAndIsBrokrageApplicable(price, propertiesWithBrokrage).stream()
				.forEach(System.out::println);

		price = (int) (1e6 * Math.random());
		type = Helper.getPropertType();
		discount = (int) (10 * Math.random());
		propertiesService.getByTypeAndPriceAndMaxDiscount(type, price, discount).stream().forEach(System.out::println);

	}

}
